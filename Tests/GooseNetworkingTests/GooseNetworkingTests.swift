    import XCTest
    @testable import GooseNetworking

    final class GooseNetworkingTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(GooseNetworking().text, "Hello, World!")
        }
    }
